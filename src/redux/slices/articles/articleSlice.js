// import { createSlice } from "@reduxjs/toolkit"
const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
    value: []
}

const articleSlice = createSlice({
   name: "articles", 
   initialState,
    reducers: {
       fetchAllArticle: (state, action) => {
            state.value = action.payload
       } 
    }
})

export const {fetchAllArticle} = articleSlice.actions
export default articleSlice.reducer