// import { articleSlice } from "../slices/articles/articleSlice";

import articleSlice from "../slices/articles/articleSlice";

const { configureStore } = require("@reduxjs/toolkit");
const { default: counterSlice } = require("../slices/counter/counterSlice");

export const store = configureStore({
    reducer: {
        counter: counterSlice,
        articles: articleSlice
    }
})