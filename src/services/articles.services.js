import { api } from "../utils/api"

export const fetch_all_article = async () => {
    const result = await api.get('articles')
    console.log(result)
    return result.data.payload
}

