import { api } from "../utils/api"

export const signin= async (username, password) => {
    const result = await api.post('login', {username, password})
    return result
}