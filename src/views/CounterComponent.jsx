import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decrement, increment } from '../redux/slices/counter/counterSlice'

export const CounterComponent = () => {
    const counter = useSelector((state) => state.counter.value)
    console.log("counter", counter)
    const dispatch = useDispatch()
  return (
    <div>
        <h1>{counter}</h1>
        <button onClick={() => dispatch(decrement())}>decrease</button>
        <button onClick={() => dispatch(increment())}>increase</button>
    </div>
  )
}
