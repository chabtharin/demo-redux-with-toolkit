import React, { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { signin } from "../../services/auth.services";

export const SignInComponent = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [token, setToken] = useState('')
    const handleUsername = (e) => {
        setUsername(e.target.value)
    }
    const handlePassword = (e) => {
        setPassword(e.target.value)
    }

    var CryptoJS = require("crypto-js");
    const handleSubmit = () => {
        const text = "my message"
        console.log("Before encrypt : ",text)
        // Encrypt
var ciphertext = CryptoJS.AES.encrypt('my message', 'secret key 123').toString();
console.log("After encrypt : ", ciphertext)
// Decrypt
var bytes  = CryptoJS.AES.decrypt(ciphertext, 'secret key 123');
var originalText = bytes.toString(CryptoJS.enc.Utf8);

console.log("After decrypt : ", originalText); // 'my message'
        signin(username, password).then(r => setToken(r.data.payload.token))
    }
    useEffect(() => {
        localStorage.setItem("token", token)
    }, [token])

    console.log("Take token out : ", localStorage.getItem("token"))
  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="text" placeholder="Enter email" onChange={handleUsername}/>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPwd">
          <Form.Label>password</Form.Label>
          <Form.Control type="password" placeholder="Enter password" onChange={handlePassword}/>
        </Form.Group>
        <Button variant="primary" onClick={handleSubmit}>
          Submit
        </Button>
      </Form>
    </div>
  );
};
