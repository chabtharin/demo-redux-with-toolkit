import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchAllArticle } from "../../redux/slices/articles/articleSlice";
import { fetch_all_article } from "../../services/articles.services";

export const ArticleComponent = () => {
  const articles = useSelector((state) => state.articles.value);
  const dispatch = useDispatch()
  console.log("Articles kkk : ", articles);
  useEffect(() => {
    fetch_all_article().then((r) => dispatch(fetchAllArticle(r)));
  });
  // const dispatch = useDispatch()
  return <div></div>;
};
