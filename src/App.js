import './App.css';
import { CounterComponent } from './views/CounterComponent';
import { Route, Routes } from 'react-router-dom';
import { SignInComponent } from './views/signin/SignInComponent';
import { ArticleComponent } from './views/article/ArticleComponent';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<ArticleComponent/>}/>
        <Route path='/signin' element={<SignInComponent/>}/>
        <Route path='/counter' element={<CounterComponent/>}/>
      </Routes>
    </div>
  );
}

export default App;
